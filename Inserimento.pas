unit Inserimento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.Mask,
  Vcl.DBCtrls, Vcl.WinXCalendars;

type
  TFMInserimento = class(TForm)
    DataSource1: TDataSource;
    CalendarPicker1: TCalendarPicker;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Button1: TButton;
    Label6: TLabel;
    Nome: TEdit;
    Cognome: TEdit;
    Data: TEdit;
    Email: TEdit;
    Telefono: TEdit;
    Email_utente: TLabel;
    Emailu: TComboBox;
    procedure CalendarPicker1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    id: String;
    bool: Boolean;

    function getlastid: Integer;
    function getid: String;
    procedure fillcombo;
  public
    property idu: String write id;
    property flag: Boolean write bool;
    { Public declarations }
  end;

var
  FMInserimento: TFMInserimento;
  idlast: Integer;

implementation

{$R *.dfm}

uses DataModule;

procedure TFMInserimento.Button1Click(Sender: TObject);
begin
  // :id,:nome,:cognome,:data,:telefono,:email,:idu,:id

  Datamodule4.inserisci.close;
  Datamodule4.inserisci.ParamByName('id').AsInteger := idlast;
  Datamodule4.inserisci.ParamByName('nome').Asstring := Nome.Text;
  Datamodule4.inserisci.ParamByName('cognome').Asstring := Cognome.Text;
  Datamodule4.inserisci.ParamByName('data').Asdate := strtodate(Data.Text);
  Datamodule4.inserisci.ParamByName('telefono').Asstring := Telefono.Text;
  Datamodule4.inserisci.ParamByName('email').Asstring := Email.Text;
  SHowMessage(idlast.ToString + ' ' + getid);
  Datamodule4.inserisci.ParamByName('idu').Asstring := getid;
  Datamodule4.inserisci.execute;
  if Emailu.Visible then
  begin
    Datamodule4.admin.Refresh;
  end
  else
  begin
    Datamodule4.Select_clienti.Refresh;
  end;
  close;
end;

procedure TFMInserimento.CalendarPicker1Change(Sender: TObject);
begin
  Data.Text := datetostr(CalendarPicker1.Date);
end;

procedure TFMInserimento.fillcombo;
var
  i: Integer;
begin
  Datamodule4.utenti.Open;
  Datamodule4.utenti.First;
  Datamodule4.utenti.Next;
  for i := 1 to Datamodule4.utenti.RecordCount - 1 do
  begin
    Emailu.AddItem(Datamodule4.utenti.FieldByName('email').Asstring, self);
    Datamodule4.utenti.Next;
  end;
end;

procedure TFMInserimento.FormCreate(Sender: TObject);
begin
  bool := false;
  Data.Text := '';
  Nome.Text := '';
  Cognome.Text := '';
  Email.Text := '';

end;

procedure TFMInserimento.FormShow(Sender: TObject);
begin
  if bool then
  begin
    Email_utente.Visible := true;
    Emailu.Visible := true;
    Emailu.Items.Clear;
    fillcombo;
    idlast := getlastid();
  end
  else
  begin
    idlast := getlastid();
  end;
  DataSource1.Enabled := true;
  Data.Text := '';
  Nome.Text := '';
  Cognome.Text := '';
  Email.Text := '';
  Telefono.Text := '';


end;

function TFMInserimento.getid: String;
begin
  if Datamodule4.utenti.Locate('Email', Emailu.Text, []) then
  begin
    result := Datamodule4.utenti.FieldByName('id').Asstring;
  end
end;

function TFMInserimento.getlastid(): Integer;
begin
  Datamodule4.admin.Last;
  result := Datamodule4.admin.FieldByName('ID').AsInteger + 1;
end;

end.
