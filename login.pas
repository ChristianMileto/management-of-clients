unit login;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TFMlogin = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Edit2: TEdit;
    Button1: TButton;
    Button2: TButton;
    Label3: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FMlogin: TFMlogin;

implementation

{$R *.dfm}

uses Home, DataModule, Registrazione, Admin_home;

procedure TFMlogin.Button1Click(Sender: TObject);
begin
  Datamodule4.login.Close;
  Datamodule4.login.ParamByName('us').AsString := Edit1.Text;
  Datamodule4.login.ParamByName('psswd').AsString := Edit2.Text;
  Datamodule4.login.Open;
  if Datamodule4.login.FieldByName('id').AsString <> '' then
  begin
      if edit1.Text = 'root' then
      begin
       FMadmin.ShowModal;
       close;
      end;

       Formgestionale.id := Datamodule4.login.FieldByName('id').AsString;
       FormGestionale.ShowModal;
       close;

  end
  else
  begin

  end;
end;

procedure TFMlogin.Button2Click(Sender: TObject);
begin
 FMRegistrazione.Show;
end;

end.
