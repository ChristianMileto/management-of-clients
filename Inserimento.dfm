object FMInserimento: TFMInserimento
  Left = 0
  Top = 0
  Caption = 'Inserimento'
  ClientHeight = 390
  ClientWidth = 443
  Color = 42495
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 36
    Top = 16
    Width = 189
    Height = 29
    Caption = 'INSERISCI I DATI'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 36
    Top = 90
    Width = 37
    Height = 13
    Caption = 'NOME :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 36
    Top = 114
    Width = 60
    Height = 13
    Caption = 'COGNOME :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 36
    Top = 141
    Width = 34
    Height = 13
    Caption = 'DATA:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 36
    Top = 178
    Width = 38
    Height = 13
    Caption = 'EMAIL:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 36
    Top = 202
    Width = 57
    Height = 13
    Caption = 'TELEFONO:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Email_utente: TLabel
    Left = 36
    Top = 237
    Width = 82
    Height = 13
    Caption = 'EMAIL UTENTE:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object CalendarPicker1: TCalendarPicker
    Left = 255
    Top = 141
    Width = 26
    Height = 28
    CalendarHeaderInfo.DaysOfWeekFont.Charset = DEFAULT_CHARSET
    CalendarHeaderInfo.DaysOfWeekFont.Color = clWindowText
    CalendarHeaderInfo.DaysOfWeekFont.Height = -13
    CalendarHeaderInfo.DaysOfWeekFont.Name = 'Segoe UI'
    CalendarHeaderInfo.DaysOfWeekFont.Style = []
    CalendarHeaderInfo.Font.Charset = DEFAULT_CHARSET
    CalendarHeaderInfo.Font.Color = clWindowText
    CalendarHeaderInfo.Font.Height = -20
    CalendarHeaderInfo.Font.Name = 'Segoe UI'
    CalendarHeaderInfo.Font.Style = []
    Color = clWindow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    OnChange = CalendarPicker1Change
    ParentFont = False
    TabOrder = 0
    TextHint = 'select a date'
  end
  object Button1: TButton
    Left = 174
    Top = 264
    Width = 75
    Height = 25
    Caption = 'Invia'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Nome: TEdit
    Left = 128
    Top = 87
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object Cognome: TEdit
    Left = 128
    Top = 114
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object Data: TEdit
    Left = 128
    Top = 141
    Width = 121
    Height = 21
    Enabled = False
    TabOrder = 4
  end
  object Email: TEdit
    Left = 128
    Top = 175
    Width = 121
    Height = 21
    TabOrder = 5
  end
  object Telefono: TEdit
    Left = 128
    Top = 202
    Width = 121
    Height = 21
    TabOrder = 6
  end
  object Emailu: TComboBox
    Left = 128
    Top = 229
    Width = 121
    Height = 21
    TabOrder = 7
    TextHint = 'Seleziona un'#39' Email'
  end
  object DataSource1: TDataSource
    AutoEdit = False
    DataSet = DataModule4.Inserisci
    Enabled = False
    Left = 568
    Top = 280
  end
end
