object Modifica: TModifica
  Left = 0
  Top = 0
  Caption = 'Modifica'
  ClientHeight = 358
  ClientWidth = 365
  Color = 42495
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 36
    Top = 8
    Width = 189
    Height = 29
    Caption = 'MODIFICA I DATI'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 36
    Top = 58
    Width = 36
    Height = 13
    Caption = 'NOME :'
  end
  object Label3: TLabel
    Left = 36
    Top = 82
    Width = 58
    Height = 13
    Caption = 'COGNOME :'
  end
  object Label4: TLabel
    Left = 36
    Top = 109
    Width = 31
    Height = 13
    Caption = 'DATA:'
  end
  object Label5: TLabel
    Left = 36
    Top = 146
    Width = 34
    Height = 13
    Caption = 'EMAIL:'
  end
  object Label6: TLabel
    Left = 36
    Top = 170
    Width = 56
    Height = 13
    Caption = 'TELEFONO:'
  end
  object Label7: TLabel
    Left = 36
    Top = 218
    Width = 31
    Height = 13
    Caption = 'NOTE:'
  end
  object Label8: TLabel
    Left = 36
    Top = 194
    Width = 81
    Height = 13
    Caption = 'STATO CLIENTE:'
  end
  object Emaill: TLabel
    Left = 36
    Top = 295
    Width = 75
    Height = 13
    Caption = 'EMAIL UTENTE:'
  end
  object Nome: TDBEdit
    Left = 128
    Top = 55
    Width = 121
    Height = 21
    DataField = 'Nome'
    DataSource = DataSource1
    TabOrder = 0
  end
  object Cognome: TDBEdit
    Left = 128
    Top = 82
    Width = 121
    Height = 21
    DataField = 'Cognome'
    DataSource = DataSource1
    TabOrder = 1
  end
  object CalendarPicker1: TCalendarPicker
    Left = 255
    Top = 109
    Width = 26
    Height = 28
    CalendarHeaderInfo.DaysOfWeekFont.Charset = DEFAULT_CHARSET
    CalendarHeaderInfo.DaysOfWeekFont.Color = clWindowText
    CalendarHeaderInfo.DaysOfWeekFont.Height = -13
    CalendarHeaderInfo.DaysOfWeekFont.Name = 'Segoe UI'
    CalendarHeaderInfo.DaysOfWeekFont.Style = []
    CalendarHeaderInfo.Font.Charset = DEFAULT_CHARSET
    CalendarHeaderInfo.Font.Color = clWindowText
    CalendarHeaderInfo.Font.Height = -20
    CalendarHeaderInfo.Font.Name = 'Segoe UI'
    CalendarHeaderInfo.Font.Style = []
    Color = clWindow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    OnChange = CalendarPicker1Change
    ParentFont = False
    TabOrder = 2
    TextHint = 'select a date'
  end
  object Data: TDBEdit
    Left = 128
    Top = 109
    Width = 121
    Height = 21
    DataField = 'Data'
    DataSource = DataSource1
    TabOrder = 3
  end
  object EMAIL: TDBEdit
    Left = 128
    Top = 143
    Width = 121
    Height = 21
    DataField = 'Email'
    DataSource = DataSource1
    TabOrder = 4
  end
  object Button1: TButton
    Left = 282
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Invia'
    TabOrder = 5
    OnClick = Button1Click
  end
  object DBEdit1: TDBEdit
    Left = 128
    Top = 167
    Width = 121
    Height = 21
    DataField = 'Telefono'
    DataSource = DataSource1
    TabOrder = 6
  end
  object DBEdit2: TDBEdit
    Left = 128
    Top = 215
    Width = 217
    Height = 66
    AutoSize = False
    DataField = 'Note'
    DataSource = DataSource1
    TabOrder = 7
  end
  object Stato_cliente: TDBEdit
    Left = 128
    Top = 191
    Width = 121
    Height = 21
    DataField = 'Stato_Cliente'
    DataSource = DataSource1
    Enabled = False
    TabOrder = 8
  end
  object scelta: TComboBox
    Left = 128
    Top = 191
    Width = 121
    Height = 21
    TabOrder = 9
    OnSelect = sceltaSelect
    Items.Strings = (
      'Confermato'
      'Rifiutato'
      'Richiamare')
  end
  object Emailu: TComboBox
    Left = 128
    Top = 287
    Width = 121
    Height = 21
    TabOrder = 10
    TextHint = 'Seleziona un'#39' Email'
  end
  object DataSource1: TDataSource
    AutoEdit = False
    DataSet = DataModule4.Select_clienti
    Left = 289
    Top = 312
  end
end
