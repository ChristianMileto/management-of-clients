object FormGestionale: TFormGestionale
  Left = 0
  Top = 0
  Caption = 'FormGestionale'
  ClientHeight = 749
  ClientWidth = 1370
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Grid_clienti: TDBGrid
    Left = 8
    Top = 36
    Width = 9997
    Height = 705
    DataSource = dsClienti
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Nome'
        ReadOnly = True
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Cognome'
        ReadOnly = True
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Telefono'
        ReadOnly = True
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Email'
        ReadOnly = True
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Data'
        ReadOnly = True
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Stato_Cliente'
        Width = 154
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Note'
        ReadOnly = True
        Width = 200
        Visible = True
      end>
  end
  object CalendarPicker1: TCalendarPicker
    Left = 168
    Top = 0
    Width = 152
    Height = 33
    CalendarHeaderInfo.DaysOfWeekFont.Charset = DEFAULT_CHARSET
    CalendarHeaderInfo.DaysOfWeekFont.Color = clWindowText
    CalendarHeaderInfo.DaysOfWeekFont.Height = -13
    CalendarHeaderInfo.DaysOfWeekFont.Name = 'Segoe UI'
    CalendarHeaderInfo.DaysOfWeekFont.Style = []
    CalendarHeaderInfo.Font.Charset = DEFAULT_CHARSET
    CalendarHeaderInfo.Font.Color = clWindowText
    CalendarHeaderInfo.Font.Height = -20
    CalendarHeaderInfo.Font.Name = 'Segoe UI'
    CalendarHeaderInfo.Font.Style = []
    Color = clWindow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    OnChange = CalendarPicker1Change
    ParentFont = False
    TabOrder = 1
    TextHint = 'select a date'
  end
  object Remove_filtrer: TBitBtn
    Left = 318
    Top = -1
    Width = 35
    Height = 33
    Caption = 'X'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    OnClick = Remove_filtrerClick
  end
  object BitBtn1: TBitBtn
    Left = 0
    Top = -4
    Width = 73
    Height = 37
    Caption = 'ADD'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 4227327
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      E6040000424DE604000000000000360000002800000014000000140000000100
      180000000000B00400006D0B00006D0B00000000000000000000FDFFFDFFFEFF
      FDFDFCFBFBF9FFFFFFEAF0E698C4A65CAC7B3FA6692AA46329A4623EA7685CAD
      7A97C4A7E8F0E6FFFFFFFBFCF9FDFEFBFFFEFFFDFFFDFFFFFFFEFEFDFEFEFDFC
      FEFDA3CDAF48A56F28A66723A8642BA25F28A15B28A25A2AA45E22A86427A668
      46A56FA2CCAFFBFEFDFEFEFDFEFEFEFEFEFFFBFDFCFDFFFEFAF8F676B48A27A2
      5C25A85C2B974E2C8D48288F472B90452B9145288F452A8C462A964E25A65B26
      A05D76B48DFAF8F7FDFFFFFBFDFDFCFDFAFEFFFE76B48A1CA35B2A9E582C8E4B
      2B944C2E974F2E924F2F8E4F2F8D4E2E924D2D974E2B934C2C8D4A2A9D581DA2
      5B76B38BFFFFFFFCFDFBFFFFFFA5CCAF279C572B9D572A93502C99552C9C5427
      964F5AA372BFD2BCC0D3BB5BA37027954E2C9A542B97542A914E2A9C55279C57
      A6CCAEFFFFFFE9EFE744A2652B9E572A97512C9C562C9F532BA05D219A546FAF
      7FEBE9ECECE9EB70AF7F2299532D9E5C2D9D522E9A542A954F289D5443A364E8
      EFE69EC5A32B9952289B582C9D5926A25E2AA3612AA66621A25B73B181EEE5E7
      EDE5E773B08021A05A2BA5652CA25F27A1592B9D54249A542898539EC4A85DA7
      71249A56279E5E2FA35B27A05B22A15B23A65E199F566AAD81EBEBEDEAEBED68
      AD81189D5522A45D219F5A279D5A2FA158269C5A2399535DA7723A9A5B2B9F60
      27A3632CA25862AA7874B78776B6856CB083A2C1A8EFEDECEFEDECA1C2A869AF
      8375B58374B58664A87C2E9E5827A05E2A9D593A9B573097503DAB6B2DAC6B2F
      9F5DCADDC9F8F7FAF8EFF1F1F1F3F4F1F1ECECEDEBEDECF1F0F0EFF1F3F7EEF1
      F8F6FACCDEC9309D5A2FA9673EA86730944F3798554CB37842B77E42A96CCADD
      CBFAF7FCF9F0F2F1F3F4F4F3F2EFF0EFEFF0EEF4F3F2F1F3F3F8F0F1F9F8FCCA
      DEC941A76942B57A4CB0743896524D9F644EBA814CBE8A4CB67A72B28B86BD99
      84BE9478B991AAC8B0F7F5F4F7F6F3ABC8AF78B99085BF9485BE9872B28A4CB5
      794CBB864EB87E4B9E6170A97C54BE8657C2935AC58E57C18B54BF8D51C58D45
      BF837DBC94F7FAF9F6F9F87DBC9345C08150C48B51BF8C57C08A5CC58D59C092
      53BA836DA77AA4C1A85FB7816ACB9D62C99860CD9A66CBA265CDA55BCB978FC4
      9DFFF9FBFFF8FB8EC39C5BCB9664CDA465CA9F62CB9964C7986CC99D5EB480A2
      C1A8E4EDE36BAD7B7BD3A56BCCA171CFA470D1A271D1AB69CD9F95C5A5FFFFFF
      FFFFFF95C4A369CC9E6FD0AA6DD1A172CEA46ECAA27DD2A56CAD7CE1EDE3FFFF
      FFAEC7AF73BC8F82DEB479CFAB7CD5AE78D9AB7AD5A784BA9AD2E6D4D2E6D484
      BA9A7AD4A679D9AB7BD4AE79CEAA88DCB376BC8FABC8B0FFFFFFFEFEFCFCFEFE
      8CB29486CCA297E5C085D5B182D9B484DEB67CCEA67EC69F7BC69E79CEA583DD
      B584D7B389D4B197E4BF8ACCA28BB294F9FEFEFEFEFCFEFCFEFFFFFEF4F6F492
      B3978BC7A1A8EBCD9EE3C792DCC091E1C090E4BD8DE4BD8FDFBF92DCC09EE2C6
      A9E9CC89C7A18FB397F3F6F4FFFFFEFDFDFEFFFEFFFDFEFDFFFFFDF9FCFCAEC8
      B28CB993A7D7B6B6E9CEB5ECD7B0EADBB4EADBB8EBD7B7E9CEA4D7B689B992AC
      C8B2F6FCFCFFFFFDFEFEFDFFFEFFFFFFFDFAFFFFFCFDFDFDFCFBFFFFFFDCECE3
      A5C2AB8EB8938DBF958EC1978BC2988CBF968FB893A9C1AAE1EBE3FFFFFFFCFD
      FBFCFDFDFEFEFFFFFFFD}
    ParentFont = False
    TabOrder = 3
    OnClick = Button1Click
  end
  object BitBtn2: TBitBtn
    Left = 383
    Top = 0
    Width = 98
    Height = 33
    Caption = 'MODIFICA...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 33023
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    OnClick = Button2Click
  end
  object dsClienti: TDataSource
    DataSet = DataModule4.Select_clienti
    Left = 440
    Top = 168
  end
  object BindSourceDB1: TBindSourceDB
    DataSource = dsClienti
    ScopeMappings = <>
    Left = 352
    Top = 224
  end
  object BindingsList1: TBindingsList
    Methods = <>
    OutputConverters = <>
    Left = 36
    Top = 277
  end
end
