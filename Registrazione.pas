unit Registrazione;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TFMRegistrazione = class(TForm)
    Label1: TLabel;
    Username: TEdit;
    Label2: TLabel;
    Password: TEdit;
    Button2: TButton;
    Label3: TLabel;
    Label4: TLabel;
    Email: TEdit;
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FMRegistrazione: TFMRegistrazione;

implementation

{$R *.dfm}

uses DataModule;

procedure TFMRegistrazione.Button2Click(Sender: TObject);
begin
  // :id,:user,:email,:password
  Datamodule4.Sign_up.Close;
  Datamodule4.Sign_up.ParamByName('id').Asinteger := Datamodule4.utenti.RecordCount + 1;
  if Datamodule4.utenti.Locate('user', Username.Text, []) then
  begin
    ShowMessage('Username gi� utilizzato');
  end
  else
  begin
    if Datamodule4.utenti.Locate('email', Email.Text, []) then
    begin
      ShowMessage('Email gi� utilizzata');
    end
    else
    begin
      Datamodule4.Sign_up.ParamByName('user').AsString := Username.Text;
      Datamodule4.Sign_up.ParamByName('email').AsString := Email.Text;
      Datamodule4.Sign_up.ParamByName('password').AsString := Password.Text;
      Datamodule4.Sign_up.Execute;
      Release;
    end;

  end;

end;

end.
