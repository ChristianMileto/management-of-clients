unit Modifica_clienti;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls,
  Vcl.WinXCalendars, Vcl.Mask, Vcl.DBCtrls;

type
  TModifica = class(TForm)
    Nome: TDBEdit;
    Cognome: TDBEdit;
    CalendarPicker1: TCalendarPicker;
    Data: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EMAIL: TDBEdit;
    Label5: TLabel;
    Button1: TButton;
    DBEdit1: TDBEdit;
    Label6: TLabel;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Stato_cliente: TDBEdit;
    scelta: TComboBox;
    Emaill: TLabel;
    Emailu: TComboBox;
    procedure CalendarPicker1Change(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure sceltaSelect(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private
    { Private declarations }
    bool: Boolean;
    procedure fillcombo;
    function getid: String;
  public
    { Public declarations }
    property flag: Boolean write bool;
  end;

var
  Modifica: TModifica;

implementation

{$R *.dfm}

uses DataModule, login;

procedure TModifica.Button1Click(Sender: TObject);
begin
  if bool = false then
  begin
    DataModule4.Select_clienti.ApplyUpdates;
  end
  else
  begin
    DataModule4.admin.FieldByName('idu').AsString := getid;
   try
    DataModule4.admin.ApplyUpdates;
   except
    Datamodule4.admin.Refresh;
   end;

  end;
  close;
end;

procedure TModifica.CalendarPicker1Change(Sender: TObject);
begin
  Data.Text := FormatDateTime('yyyy-mm-dd', CalendarPicker1.Date);
end;

procedure TModifica.fillcombo;
var
  i: Integer;
begin
  Emailu.Text := datamodule4.admin.FieldByName('Email_Utente').AsString;
  DataModule4.utenti.Open;
  Datamodule4.utenti.First;
  DataModule4.utenti.Next;
  for i := 1 to DataModule4.utenti.RecordCount - 1 do
  begin
    Emailu.AddItem(DataModule4.utenti.FieldByName('email').AsString, self);
    DataModule4.utenti.Next;
  end;
end;

procedure TModifica.FormCreate(Sender: TObject);
begin
 if  Datamodule4.login.FieldByName('id').AsString = '0' then
 begin
   flag := true;
 end
 else
 begin
   flag := false;
 end;

end;

procedure TModifica.FormShow(Sender: TObject);
begin

  CalendarPicker1.DateFormat := 'yyyy-mm-dd';
  scelta.Text := Stato_cliente.Text;
  if bool = false then
  begin
    DataSource1.DataSet := DataModule.DataModule4.Select_clienti;
    DataModule.DataModule4.Select_clienti.edit;
  end
  else
  begin
    DataSource1.DataSet := DataModule.DataModule4.admin;
    DataModule.DataModule4.admin.edit;

    Emaill.Visible := true;
    Emailu.Visible := true;
    Emailu.Items.Clear;
      fillcombo;
  end;
end;

function TModifica.getid: String;
begin
  if DataModule4.utenti.Locate('Email', Emailu.Text, []) then
  begin
    result := DataModule4.utenti.FieldByName('id').AsString;
  end
end;

procedure TModifica.sceltaSelect(Sender: TObject);
begin
  Stato_cliente.Text := scelta.Text;
end;

end.
