unit Admin_home;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Vcl.WinXCalendars, Vcl.StdCtrls, Vcl.Buttons;

type
  TFMadmin = class(TForm)
    BitBtn1: TBitBtn;
    CalendarPicker1: TCalendarPicker;
    Remove_filtrer: TBitBtn;
    BitBtn2: TBitBtn;
    DataSource1: TDataSource;
    Grid_clienti: TDBGrid;
    procedure CalendarPicker1Change(Sender: TObject);
    procedure Remove_filtrerClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FMadmin: TFMadmin;

implementation

{$R *.dfm}

uses DataModule, Inserimento, Modifica_clienti, Registrazione;

procedure TFMadmin.BitBtn1Click(Sender: TObject);
begin
  FMInserimento.flag := true;
  FMInserimento.Show;
end;

procedure TFMadmin.BitBtn2Click(Sender: TObject);
begin
  Tmodifica.Create(self).Show;


end;

procedure TFMadmin.CalendarPicker1Change(Sender: TObject);
begin
  CalendarPicker1.DateFormat := 'yyyy-mm-dd';
  DataModule4.admin.Filtered := false;
  DataModule4.admin.Filter := ' Data LIKE ' +
    QuotedStr(datetostr(CalendarPicker1.Date));
  DataModule4.admin.Filtered := true;
  Remove_filtrer.Show;
end;

procedure TFMadmin.FormCreate(Sender: TObject);
begin
  Grid_clienti.Refresh;
  Grid_clienti.SelectedIndex := 1;
end;

procedure TFMadmin.Remove_filtrerClick(Sender: TObject);
begin
  DataModule4.admin.Filtered := false;
  Remove_filtrer.Hide;
end;

end.
