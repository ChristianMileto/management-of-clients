 # Management of clients 
 
 
It’s an application that helps users manage their appointments with customers.
The user can create and edit new appointments while also leaving notes.
The admin will also see all customers and users connected to it and can also create and edit new appointments with customers by connecting the latter to a user.
![](Images/Img1.png)
![](Images/Img2.png)
![](Images/Img3.png)
![](Images/Img4.png)
