program Project9;

uses
  Vcl.Forms,
  Home in 'Home.pas' {FormGestionale},
  DataModule in 'DataModule.pas' {DataModule4: TDataModule},
  Inserimento in 'Inserimento.pas' {FMInserimento},
  Modifica_clienti in 'Modifica_clienti.pas' {Modifica},
  login in 'login.pas' {FMlogin},
  Registrazione in 'Registrazione.pas' {FMRegistrazione},
  Admin_home in 'Admin_home.pas' {FMadmin};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFMlogin, FMlogin);
  Application.CreateForm(TDataModule4, DataModule4);
  Application.CreateForm(TFormGestionale, FormGestionale);
  Application.CreateForm(TFMInserimento, FMInserimento);
  Application.CreateForm(TFMRegistrazione, FMRegistrazione);
  Application.CreateForm(TFMadmin, FMadmin);
  Application.Run;
end.
